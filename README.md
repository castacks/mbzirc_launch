**This explains how to install the MBZIRC environment on the DJI Manifold on-board computer and how to configure and run the DJI Matrice 100 controller**

**OBS:** To install the desktop version of the environment, which includes the simulator, please refer to the [mbzir_simulation package](https://bitbucket.org/castacks/mbzirc_simulation).

**NOTE**: It uses **ROS Indigo Base (bare bones)** version for Ubuntu Arm. It could be found on this [link](http://wiki.ros.org/indigo/Installation/UbuntuARM)

### Configuration ###

For stability remove the hud service on the manifold. In a terminal:

```
sudo chmod -x /usr/lib/arm-linux-gnueabihf/hud/hud-service #to revert that for some reason just use +x instead of -x
sudo pkill -9 hud-service

```


### Installation ###

Install wstool:

```
sudo apt-get install python-wstool # get wstool
```


Create a workspace and install the packages:


```
source /opt/ros/indigo/setup.bash # init environment
mkdir -p mbzirc_ws/src
cd mbzirc_ws/src
catkin_init_workspace
git clone git@bitbucket.org:castacks/metarepository.git
ln -s metarepository/rosinstall/mbzirc_onboard.rosinstall .rosinstall
wstool info # see status
wstool up # get stuff

```

Install dependencies:


```
./mbzirc_mission/install_dependencies.sh 
./ca_common/install_dependencies.sh
./mikrokopter/install_dependencies.sh

```

There is a library **libdcam.so** which is provided by DJI. The library is pre-compiled for DJI Manifold (i.e. ARM-based processors) and will not work with other CPUs. If you are trying to install the environment on your computer, you should tell *catkin* to ignore compiling *dji_sdk_read_x3_cam* package, which depends on the library. This can be done with the following command:

```
touch dji_sdk_read_x3_cam/CATKIN_IGNORE
```


Compile:


```
cd ..
catkin_make
```

There shouldn't be any errors due to non-compilation of messages. However, if errors come up, run `catkin_make` again to fix it. If this is not sufficient, run `catkin_make --pkg name_of_the_package_with_missing_message` to force the message to be generated before it is needed. Please report the issue to the maintainers.

Another common error while compiling on DJI manifold is similar to:

```
c++: internal compiler error: Killed (program cc1plus)
Please submit a full bug report, with preprocessed source if appropriate
```
This happens because the Manifold is out of memory. The easiest solution to this is to reboot the computer and restart `catkin_make`.  

### Configuration and launching ###

The following file may need to be changed before launching the control system:

* dji_sdk.launch - change to modify DJI developer credentials.
* dji_control.launch - change the parameters of the controller.
* <name of mission>.launch - change the TF that represents the pose of the track in relation to the world reference frame and the quad constraints during the flight.

In a terminal run:

```
source devel/setup.bash
roslaunch mbzirc_launch <name of mission>.launch
```


On dji remote flick the mode switch to F-mode to start autonomous trajectory following.


### To get images from the Gimbal ###

Install the dependencies:


```
sudo apt-get install ros-indigo-image-transport
sudo apt-get install ros-indigo-cv-bridge
sudo apt-get install ros-indigo-image-view  # this is just to see the published images

```


Compile the camera node:


```
cd ~/mbzirc_ws/
source devel/setup.bash
rm src/airlab_dji_sdk/dji_sdk_manifold_read_cam/CATKIN_IGNORE
catkin_make

```
Configure the display and enter the sudo mode by running the following script: 

```
./src/mbzirc_launch/scripts/cameraconf.sh

```

Launch the node:


```
roslaunch mbzirc_launch manifold_cam.launch

```

IMPORTANT: You will several errors of the form:


```
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
---> TVMR: Video-conferencing detected !!!!!!!!!
[h264 @ 0xad9c3000] non-existing PPS 0 referenced
[h264 @ 0xad9c3000] non-existing PPS 0 referenced

```

Those errors are not important. However, you **must** see the line `---> TVMR: Video-conferencing detected !!!!!!!!!`. If you did not get this line, you have a problem with the display. In this case, try to run:


```
export DISPLAY=:0.0
```




To see the images, in another terminal run:


```
rosrun image_view image_view image:=dji_sdk/image_raw

```



###Running the system without Ethernet connection during the flight###

Connect to the manifold directly, using keyboard, mouse and monitor, or remotely, using ssh. 

In a terminal run:

```
screen -S sectionname

```
Start the processes you want. For example, launch the camera node:

```
roslaunch mbzirc_launch manifold_cam.launch

```

Press `Ctrl-A` then `Ctrl-D`. This will "detach" your screen session but leave your processes running.

You may now close the terminal or logout from the remote section. Your process will keep running. You may also, in the same terminal, start a new screen section using `screen -S sectionname2` and launch new processes.

Once the robot is back, you may log into it and resume the section by typing:
 
```
screen -r sectionname
```

If you have many sections you can see them all by typing:

```
screen -r 
```