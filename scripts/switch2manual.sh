#!/bin/bash

rosparam set /mission/authorized false

rostopic pub --once /fake_spektrum/status spektrum/MikrokopterStatus  '{header: auto, isAutonomous: 0}'

